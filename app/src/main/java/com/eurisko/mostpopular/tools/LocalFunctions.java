package com.eurisko.mostpopular.tools;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;

import com.eurisko.mostpopular.BuildConfig;
import com.eurisko.mostpopular.activities.NewsDetailsActivity;
import com.eurisko.mostpopular.beans.NewsBean;
import com.eurisko.mostpopular.components.AlertDialogFragment;
import com.eurisko.mostpopular.components.ProgressDialogFragment;

public class LocalFunctions {


    private static final String LOG_TAG = "MostPopular";


    public static ProgressDialogFragment getProgressDialog(Activity activity, String msg) {
        ProgressDialogFragment diag = new ProgressDialogFragment();
        diag.setMessage(msg);
        return diag;
    }

    public static void showDialog(final Activity activity, String message) {
        try {
            AlertDialogFragment diag = new AlertDialogFragment();
            diag.setMessage(message);
            diag.setCancelable(false);
            diag.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            if (!activity.isFinishing())
                diag.show(activity.getFragmentManager(), null);
        } catch (Exception e) {
            printException(e);
        }

    }

    public static void showDialog(final Activity activity, String message, final Runnable runnable) {
        try {
            AlertDialogFragment diag = new AlertDialogFragment();
            diag.setMessage(message);
            diag.setCancelable(false);
            diag.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (runnable != null) {
                        new Handler().post(runnable);
                    }
                    dialog.dismiss();
                }
            });
            diag.setOnDismissListener(new AlertDialogFragment.OnDismissListener() {
                @Override
                public void onDismiss() {
                    if (runnable != null) {
                        new Handler().post(runnable);
                    }
                }
            });
            if (!activity.isFinishing())
                diag.show(activity.getFragmentManager(), null);
        } catch (Exception e) {
            printException(e);
        }

    }

    public static void showDialogActions(final Activity activity, String message, String positiveBtn, String negativeBtn, final Runnable positiveRunnable, final Runnable negativeRunnable) {
        try {
            AlertDialogFragment diag = new AlertDialogFragment();
            diag.setMessage(message);
            diag.setCancelable(false);
            diag.setPositiveButton(positiveBtn, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (positiveRunnable != null) {
                        new Handler().post(positiveRunnable);
                    }
                    dialog.dismiss();
                }
            });
            diag.setNegativeButton(negativeBtn, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (negativeRunnable != null) {
                        new Handler().post(negativeRunnable);
                    }
                    dialog.dismiss();
                }
            });
            diag.setOnDismissListener(new AlertDialogFragment.OnDismissListener() {
                @Override
                public void onDismiss() {
                    if (negativeRunnable != null) {
                        new Handler().post(negativeRunnable);
                    }
                }
            });
            if (!activity.isFinishing())
                diag.show(activity.getFragmentManager(), null);
        } catch (Exception e) {
            printException(e);
        }

    }

    public static void setRefreshing(final SwipeRefreshLayout swipeRefreshLayout, final boolean isRefreshing) {
        if (swipeRefreshLayout != null)
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        swipeRefreshLayout.setRefreshing(isRefreshing);
                    } catch (Exception e) {
                    }
                }
            });
    }

    public static int dip2pix(int dip, Context activity) {
        float scale = activity.getResources().getDisplayMetrics().density;
        return Math.round(dip * scale);
    }

    public static int getScreenWidth(Activity activity){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }
    public static void printException(Exception e) {
        if (BuildConfig.DEBUG) {
            if (e != null) {
                Log.d(LOG_TAG, Log.getStackTraceString(e));
            }
        }
    }

    public static void goToNewsDetails(Activity activity, NewsBean newsBean) {
        Intent intent = new Intent(activity, NewsDetailsActivity.class);
        intent.putExtra(Variables.BUNDLE_NEWS_BEAN, newsBean);
        activity.startActivity(intent);
    }
}
