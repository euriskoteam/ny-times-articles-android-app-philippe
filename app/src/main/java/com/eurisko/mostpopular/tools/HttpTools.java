package com.eurisko.mostpopular.tools;


import com.eurisko.mostpopular.beans.ResponseBean;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HttpTools {

    public static ResponseBean getHttp(String urlStr, HashMap<String, String> headers) throws Exception {
        InputStream inputStream;
        String result = "";
        ResponseBean response = new ResponseBean();

        URL url = new URL(urlStr);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(Variables.VAL_TIME_OUT);
        connection.setConnectTimeout(Variables.VAL_TIME_OUT);
        connection.setRequestMethod("GET");
        connection.setDoInput(true);

        if (headers != null) {
            Iterator it = headers.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> pair = (Map.Entry) it.next();
                connection.setRequestProperty(pair.getKey(), pair.getValue());
                it.remove();
            }
        }

        connection.connect();

        if (connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST)
            inputStream = connection.getInputStream();
        else
            inputStream = connection.getErrorStream();

        if (inputStream != null) {
            result = PrintStream(inputStream);
            inputStream.close();
        } else {
            result = "";
        }

        response.setCode(connection.getResponseCode());
        response.setResponse(result);

        return response;
    }


    public static String PrintStream(InputStream in) throws Exception {
        InputStreamReader isr = new InputStreamReader(in, Charset.forName("UTF-8"));
        BufferedReader buffer = new BufferedReader(isr);
        String str = "";
        StringBuilder strbuild = new StringBuilder();
        while ((str = buffer.readLine()) != null) {
            strbuild.append(str + "\n");
        }
        return strbuild.toString();

    }


}
