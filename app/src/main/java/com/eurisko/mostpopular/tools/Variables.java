package com.eurisko.mostpopular.tools;

public class Variables {

    public static final String BUNDLE_NEWS_BEAN = "newsBean";

    public static final int VAL_TIME_OUT = 15000;

    public static final String VAL_API_KEY = "Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5";
    public static final String VAL_ALL_SECTIONS = "all-sections";
    public static final String VAL_KEY_PERIOD_WEEKLY = "7";

    public static final String GET_NYTIMES_API(String section, String period) {
        return "http://api.nytimes.com/svc/mostpopular/v2/mostviewed/" + section + "/" + period + ".json?api-key=" + VAL_API_KEY;
    }

}
