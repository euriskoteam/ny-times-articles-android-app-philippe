package com.eurisko.mostpopular.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurisko.mostpopular.R;
import com.eurisko.mostpopular.beans.NewsBean;
import com.eurisko.mostpopular.tools.LocalFunctions;
import com.eurisko.mostpopular.tools.ImageLoadingTools;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {


    Activity activity;
    ArrayList<NewsBean> newsBeans;
    LayoutInflater inflater;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    public NewsAdapter(Activity activity, ArrayList<NewsBean> newsBeans) {
        this.activity = activity;
        this.newsBeans = newsBeans;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = ImageLoadingTools.getImageLoader(activity);
        options = ImageLoadingTools.getImageOptions(R.mipmap.ic_launcher);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = inflater.inflate(R.layout.item_news, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        NewsBean bean = newsBeans.get(i);

        holder.txtTitle.setText(bean.getTitle());
        holder.txtPublishDate.setText(bean.getPublishedDate());
        holder.txtByLine.setText(bean.getByLine());
        imageLoader.displayImage(bean.getThumbnail(), holder.imgNews, options);

        holder.relNewsItem.setTag(i);
        holder.relNewsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewsBean newsBean = newsBeans.get((Integer) v.getTag());
                LocalFunctions.goToNewsDetails(activity, newsBean);
            }
        });


    }

    @Override
    public int getItemCount() {
        return newsBeans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle, txtPublishDate, txtByLine;
        public ImageView imgNews;
        public RelativeLayout relNewsItem;

        public ViewHolder(View view) {
            super(view);
            txtTitle = view.findViewById(R.id.txtTitle);
            txtPublishDate = view.findViewById(R.id.txtPublishDate);
            txtByLine = view.findViewById(R.id.txtByLine);
            imgNews = view.findViewById(R.id.imgNews);
            relNewsItem = view.findViewById(R.id.relNewsItem);
        }
    }

}
