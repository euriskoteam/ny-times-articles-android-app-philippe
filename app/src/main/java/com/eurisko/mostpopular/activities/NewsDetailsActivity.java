package com.eurisko.mostpopular.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eurisko.mostpopular.R;
import com.eurisko.mostpopular.beans.NewsBean;
import com.eurisko.mostpopular.tools.LocalFunctions;
import com.eurisko.mostpopular.tools.Variables;
import com.eurisko.mostpopular.tools.ImageLoadingTools;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class NewsDetailsActivity extends AppCompatActivity {

    Activity activity;


    ImageView imgNews;
    TextView txtTitle, txtDescription, txtByLine, txtPublishDate;

    NewsBean newsBean;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        setupViews();
        implementViews();
    }

    private void setupViews() {
        imgNews = findViewById(R.id.imgNews);
        txtDescription = findViewById(R.id.txtDescription);
        txtTitle = findViewById(R.id.txtTitle);
        txtByLine = findViewById(R.id.txtByLine);
        txtPublishDate = findViewById(R.id.txtPublishDate);
        newsBean = (NewsBean) getIntent().getSerializableExtra(Variables.BUNDLE_NEWS_BEAN);
        imageLoader = ImageLoadingTools.getImageLoader(activity);
        options = ImageLoadingTools.getImageOptions(R.mipmap.ic_launcher);
    }

    private void implementViews() {
        imageLoader.displayImage(newsBean.getImage(), imgNews, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                imgNews.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imgNews.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                try {
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imgNews.getLayoutParams();
                    int desiredHeight;
                    int actualWidth = LocalFunctions.getScreenWidth(activity);
                    desiredHeight = actualWidth * loadedImage.getHeight() / loadedImage.getWidth();
                    params.height = desiredHeight;
                    imgNews.setLayoutParams(params);
                } catch (Exception e) {
                    LocalFunctions.printException(e);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                imgNews.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
        });
        txtTitle.setText(newsBean.getTitle());
        txtByLine.setText(newsBean.getByLine());
        txtPublishDate.setText(newsBean.getPublishedDate());
        txtDescription.setText(newsBean.getDescription());
    }
}
