package com.eurisko.mostpopular.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.eurisko.mostpopular.R;
import com.eurisko.mostpopular.adapters.NewsAdapter;
import com.eurisko.mostpopular.asyncs.GetMostPopularNewsAsync;
import com.eurisko.mostpopular.beans.NewsBean;
import com.eurisko.mostpopular.components.RetryableAsyncTask;
import com.eurisko.mostpopular.tools.LocalFunctions;

import java.util.ArrayList;

public class NewsActivity extends AppCompatActivity {

    Activity activity;

    SwipeRefreshLayout swpRefreshNews;
    RecyclerView rclNews;
    TextView txtNoDataFound;


    GetMostPopularNewsAsync async;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        setupViews();

        implementViews();

    }


    private void setupViews() {
        swpRefreshNews = findViewById(R.id.swpRefreshNews);
        rclNews = findViewById(R.id.rclNews);
        txtNoDataFound = findViewById(R.id.txtNoDataFound);
    }


    private void implementViews() {
        swpRefreshNews.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchNews();
            }
        });
        fetchNews();
    }


    private void fetchNews() {
        LocalFunctions.setRefreshing(swpRefreshNews, true);
        async = new GetMostPopularNewsAsync(activity, new RetryableAsyncTask.OnFinishListener() {
            @Override
            public void onSuccess(Object object) {
                LocalFunctions.setRefreshing(swpRefreshNews, false);
                try {
                    addNews((ArrayList<NewsBean>) object);
                } catch (Exception e) {
                    LocalFunctions.printException(e);
                }

            }

            @Override
            public void onError(Object msg) {
                LocalFunctions.setRefreshing(swpRefreshNews, false);
            }
        });

        async.setOnNoInternetConnection(new RetryableAsyncTask.OnNoInternetConnection() {
            @Override
            public void onNoInternet() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        LocalFunctions.setRefreshing(swpRefreshNews,false);
                    }
                },200);

            }
        });
        async.setRetryRunnable(new Runnable() {
            @Override
            public void run() {
                LocalFunctions.setRefreshing(swpRefreshNews,true);
            }
        });
        async.setFinishActivity(false);
        async.setCheckConnection(true);
        async.setWithDialog(false);
        async.executeAsync();
    }

    public void addNews(ArrayList<NewsBean> newsBeans) {
        try {
            if (newsBeans.size() == 0) {
                txtNoDataFound.setVisibility(View.VISIBLE);
            } else {
                txtNoDataFound.setVisibility(View.GONE);
            }

            NewsAdapter adapter = new NewsAdapter(activity, newsBeans);




            rclNews.setLayoutManager(new LinearLayoutManager(activity));
            rclNews.setAdapter(adapter);

        } catch (Exception e) {
            LocalFunctions.printException(e);
        }
    }
}
