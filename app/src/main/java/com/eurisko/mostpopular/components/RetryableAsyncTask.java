package com.eurisko.mostpopular.components;

import android.app.Activity;
import android.os.AsyncTask;

import com.eurisko.mostpopular.beans.ResponseBean;
import com.eurisko.mostpopular.tools.ConnectionTools;
import com.eurisko.mostpopular.tools.LocalFunctions;

import java.io.InterruptedIOException;


public abstract class RetryableAsyncTask extends AsyncTask<Object, Object, Object> {


    public abstract void doBackGroundWork() throws Exception;

    public abstract AsyncTask getRetryAsync();

    public Activity activity;
    public boolean isRunning = false;

    protected boolean withDialog = false;

    String msg = "Loading...";
    String noConnectionMsg = "No internet connection. Please check your network settings and try again.";

    Runnable noConnectionRunnable = null;
    Runnable retryRunnable = null;

    protected ProgressDialogFragment dialog;
    public OnFinishListener mOnFinishListener;
    OnNoInternetConnection onNoInternetConnection;

    protected ResponseBean response;


    boolean isParallel = false;

    public RetryableAsyncTask(Activity activity, OnFinishListener mOnFinishListener) {
        this.activity = activity;
        this.mOnFinishListener = mOnFinishListener;
        this.response = new ResponseBean();
    }


    public RetryableAsyncTask setProgressDialogMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public RetryableAsyncTask setWithDialog(boolean withDialog) {
        this.withDialog = withDialog;
        return this;
    }

    public RetryableAsyncTask setCheckConnection(boolean checkConnection) {
        this.checkConnection = checkConnection;
        return this;
    }

    public RetryableAsyncTask setShowRetryOnNoConnection(boolean showRetryOnNoConnection) {
        this.showRetryOnNoConnection = showRetryOnNoConnection;
        return this;
    }

    public RetryableAsyncTask setShowRetryOnFail(boolean showRetryOnFail) {
        this.showRetryOnFail = showRetryOnFail;
        return this;
    }

    public RetryableAsyncTask setFinishActivity(boolean finishActivity) {
        this.finishActivity = finishActivity;
        return this;
    }

    public RetryableAsyncTask setNoConnectionMessage(String noConnectionMsg) {
        this.noConnectionMsg = noConnectionMsg;
        return this;
    }

    public RetryableAsyncTask setNoConnectionRunnable(Runnable runnable) {
        this.noConnectionRunnable = runnable;
        return this;
    }

    public RetryableAsyncTask setRetryRunnable(Runnable runnable) {
        this.retryRunnable = runnable;
        return this;
    }

    public Runnable getNoConnectionRunnable() {
        return noConnectionRunnable;
    }

    public Runnable getRetryRunnable() {
        return retryRunnable;
    }

    public boolean isShowRetryOnNoConnection() {
        return showRetryOnNoConnection;
    }

    public boolean isShowNoConnectionMsg() {
        return showNoConnectionMsg;
    }

    public void setShowNoConnectionMsg(boolean showNoConnectionMsg) {
        this.showNoConnectionMsg = showNoConnectionMsg;
    }


    public boolean isParallel() {
        return isParallel;
    }

    public void setParallel(boolean parallel) {
        isParallel = parallel;
    }

    public boolean isShowRetryOnFail() {
        return showRetryOnFail;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        isRunning = true;
        try {
            if (withDialog) {
                dialog = LocalFunctions.getProgressDialog(activity, msg);
                dialog.show(activity.getFragmentManager(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected Object doInBackground(Object... params) {
        try {
            doBackGroundWork();
        } catch (Exception e) {
            e.printStackTrace();
            if (!(e instanceof InterruptedException) && !(e instanceof InterruptedIOException)) {
                if (showRetryOnFail) {
                    if (!activity.isFinishing())
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (getRetryAsync() != null)
                                    ConnectionTools.retryDialog(activity, getRetryAsync(), checkConnection, finishActivity, withDialog, msg, retryRunnable, onNoInternetConnection);
                            }
                        });
                }
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        isRunning = false;
        try {
            if (withDialog && !activity.isFinishing())
                dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean checkConnection = true;
    boolean showRetryOnNoConnection = false;
    boolean showNoConnectionMsg = true;
    boolean showRetryOnFail = true;
    boolean finishActivity = false;

    //execute async with internet connection check and to finish activity on retry dialog
    public void executeAsync() {
        if (!checkConnection) {
            this.execute();
        } else {
            if (ConnectionTools.CheckNetwork(activity)) {
                this.execute();
            } else {
                if (onNoInternetConnection != null) {
                    this.onNoInternetConnection.onNoInternet();
                }
                if (showRetryOnNoConnection) {
                    if (getRetryAsync() != null)
                        ConnectionTools.retryDialog(activity, getRetryAsync(), checkConnection, finishActivity, withDialog, msg, onNoInternetConnection);
                } else {
                    if (showNoConnectionMsg) {
                        LocalFunctions.showDialog(activity, noConnectionMsg, noConnectionRunnable);
                    }
                }
            }
        }
    }

    //execute async with internet connection check and to finish activity on retry dialog
    public void executeAsyncParallel() {
        isParallel = true;
        if (!checkConnection) {
            this.execute();
        } else {
            if (ConnectionTools.CheckNetwork(activity)) {
                this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                if (onNoInternetConnection != null) {
                    this.onNoInternetConnection.onNoInternet();
                }
                if (showRetryOnNoConnection) {
                    if (getRetryAsync() != null)
                        ConnectionTools.retryDialog(activity, getRetryAsync(), checkConnection, finishActivity, withDialog, msg, onNoInternetConnection);
                } else {
                    if (showNoConnectionMsg) {
                        LocalFunctions.showDialog(activity, noConnectionMsg, noConnectionRunnable);
                    }
                }
            }
        }
    }

    public RetryableAsyncTask setOnNoInternetConnection(OnNoInternetConnection onNoInternetConnection) {
        this.onNoInternetConnection = onNoInternetConnection;
        return this;
    }

    public OnNoInternetConnection getOnNoInternetConnection() {
        return this.onNoInternetConnection;
    }

    public interface OnFinishListener {
        void onSuccess(Object object);

        void onError(Object msg);
    }

    /**
     * in case not connected to any network...(before execute async)
     */
    public interface OnNoInternetConnection {
        void onNoInternet();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        isRunning = false;
    }

    @Override
    protected void onCancelled(Object o) {
        super.onCancelled(o);
        isRunning = false;
    }

}
