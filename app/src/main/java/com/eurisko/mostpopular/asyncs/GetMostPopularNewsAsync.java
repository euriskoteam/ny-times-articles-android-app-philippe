package com.eurisko.mostpopular.asyncs;

import android.app.Activity;
import android.os.AsyncTask;

import com.eurisko.mostpopular.beans.NewsBean;
import com.eurisko.mostpopular.beans.ResponseBean;
import com.eurisko.mostpopular.components.RetryableAsyncTask;
import com.eurisko.mostpopular.parsers.NewsParsers;
import com.eurisko.mostpopular.tools.Variables;
import com.eurisko.mostpopular.tools.HttpTools;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class GetMostPopularNewsAsync extends RetryableAsyncTask {

    Activity activity;
    ResponseBean response;
    boolean isError = true;
    ArrayList<NewsBean> newsBeans = new ArrayList<>();

    public GetMostPopularNewsAsync(Activity activity, OnFinishListener onFinishListener) {
        super(activity, onFinishListener);
        this.activity = activity;
    }

    @Override
    public void doBackGroundWork() throws Exception {
        String url = Variables.GET_NYTIMES_API(Variables.VAL_ALL_SECTIONS, Variables.VAL_KEY_PERIOD_WEEKLY);
        response = HttpTools.getHttp(url, new HashMap<String, String>());
        if (response.getCode() != 200)
            isError = true;
        else {
            JSONObject jsonObject = new JSONObject(response.getResponse());
            if (jsonObject.has("status")) {
                String status = jsonObject.getString("status");
                if (status.equals("OK")) {
                    isError = false;
                    newsBeans = NewsParsers.getNews(response.getResponse());
                } else {
                    isError = true;
                }
            }
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if (mOnFinishListener != null) {
            if (isError) {
                mOnFinishListener.onError("");
            } else {
                mOnFinishListener.onSuccess(newsBeans);
            }
        }
    }

    @Override
    public AsyncTask getRetryAsync() {
        return new GetMostPopularNewsAsync(activity, mOnFinishListener);
    }
}
