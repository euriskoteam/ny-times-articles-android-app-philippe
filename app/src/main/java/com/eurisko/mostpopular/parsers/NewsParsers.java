package com.eurisko.mostpopular.parsers;

import com.eurisko.mostpopular.beans.NewsBean;
import com.eurisko.mostpopular.tools.LocalFunctions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewsParsers {

    public static ArrayList<NewsBean> getNews(String response) {
        ArrayList<NewsBean> newsBeans = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("results")) {
                JSONArray newsArray = jsonObject.getJSONArray("results");
                for (int i = 0; i < newsArray.length(); i++) {
                    NewsBean newsBean = new NewsBean();
                    JSONObject newsObject = newsArray.getJSONObject(i);
                    if (newsObject.has("title") && !newsObject.isNull("title")) {
                        newsBean.setTitle(newsObject.getString("title"));
                    }
                    if (newsObject.has("byline") && !newsObject.isNull("byline")) {
                        newsBean.setByLine(newsObject.getString("byline"));
                    }
                    if (newsObject.has("published_date") && !newsObject.isNull("published_date")) {
                        newsBean.setPublishedDate(newsObject.getString("published_date"));
                    }
                    if (newsObject.has("abstract") && !newsObject.isNull("abstract")) {
                        newsBean.setDescription(newsObject.getString("abstract"));
                    }
                    if (newsObject.has("media") && !newsObject.isNull("media")) {
                        try {
                            JSONArray mediaArray = newsObject.getJSONArray("media");
                            JSONObject mediaObject = mediaArray.getJSONObject(0);
                            if (mediaObject.has("type") && mediaObject.has("subtype") &&
                                    mediaObject.getString("type").equals("image") &&
                                    mediaObject.getString("subtype").equals("photo")) {
                                if (mediaObject.has("media-metadata") && !mediaObject.isNull("media-metadata")) {
                                    JSONArray images = mediaObject.getJSONArray("media-metadata");
                                    for (int j = 0; j < images.length(); j++) {
                                        JSONObject image = images.getJSONObject(j);
                                        if (image.has("format") && image.getString("format").equals("mediumThreeByTwo440")) {
                                            if (image.has("url") && !image.isNull("url")) {
                                                newsBean.setImage(image.getString("url"));
                                            }
                                        }
                                        if (image.has("format") && image.getString("format").equals("Large Thumbnail")) {
                                            if (image.has("url") && !image.isNull("url")) {
                                                newsBean.setThumbnail(image.getString("url"));
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            LocalFunctions.printException(e);
                        }
                    }
                    newsBeans.add(newsBean);
                }
            }
        } catch (Exception e) {
            LocalFunctions.printException(e);
        }
        return newsBeans;
    }
}
