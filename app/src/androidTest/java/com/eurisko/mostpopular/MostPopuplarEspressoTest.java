package com.eurisko.mostpopular;

import android.app.Activity;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.eurisko.mostpopular.activities.NewsActivity;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class MostPopuplarEspressoTest {


    private int itemCount = 0;
    Activity activity;
    RecyclerView recyclerView;

    @Rule
    public ActivityTestRule<NewsActivity> mActivityRule =
            new ActivityTestRule<>(NewsActivity.class);


    @Before
    public void init(){
        this.activity = mActivityRule.getActivity();
        this.recyclerView = activity.findViewById(R.id.rclNews);
    }

    @Test
    public void recyclerViewNewsClickTest() {
        Espresso.onView(ViewMatchers.withId(R.id.swpRefreshNews))
                .perform(ViewActions.swipeDown());


        this.itemCount = recyclerView.getAdapter().getItemCount();
        for (int i = 0; i < itemCount; i++) {
            Espresso.onView(ViewMatchers.withId(R.id.rclNews)).
                    check(hasItemsCount()).perform(RecyclerViewActions.actionOnItemAtPosition(
                    i, ViewActions.click()));
            Espresso.pressBack();
        }

    }


    public static ViewAssertion hasItemsCount() {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView rv = (RecyclerView) view;
                Assert.assertTrue(rv.getAdapter().getItemCount() > 0);
            }
        };
    }


}
